﻿using netExam.Models;

namespace netExam.Logic;

public class CountService
{
    public int Count(Questionnaire questionnaire)
    {
        var count = 0;

        count += questionnaire.Age switch
        {
            >= 21 and <= 28 => questionnaire.Sum switch
            {
                < 1000000 => 12,
                < 3000000 => 9,
                _ => 0
            },
            >= 29 and <= 59 => 14,
            >= 60 and <= 72 => questionnaire.Pledge is not Pledge.WithoutPledge ? 8 : 0,
            _ => 0,
        };

        count += questionnaire.isCriminal ? 0 : 15;

        count += questionnaire.Goal switch
        {
            Goal.OnLending => 12,
            Goal.RealEstate => 8,
            Goal.Consumer => 14,
            _ => throw new ArgumentOutOfRangeException(nameof(questionnaire), questionnaire.Goal,
                "no goal")
        };

        count += questionnaire.Pledge switch
        {
            Pledge.RealEstate => 14,
            Pledge.Guarantee => 12,
            Pledge.Auto => questionnaire.AutoAge < 3 ? 8 : 3,
            Pledge.WithoutPledge => 0,
            _ => throw new ArgumentOutOfRangeException(nameof(questionnaire), questionnaire.Pledge,
                "no pledge")
        };

        count += questionnaire.OtherCredits ? 0 : questionnaire.Goal is Goal.OnLending ? 0 : 15;

        count += questionnaire.Sum switch
        {
            <= 1000000 => 12,
            <= 5000000 => 14,
            <= 10000000 => 8,
            _ => 0,
        };
        
        count += questionnaire.Employment switch
        {
            Employment.Unemployed => 0,
            Employment.Ip => 12,
            Employment.Tk => 14,
            Employment.NonTk => 8,
            Employment.Old => questionnaire.Age < 70 ? 5 : 0,
            _ => throw new ArgumentOutOfRangeException(nameof(questionnaire), questionnaire.Employment,
                "not emp yet")
        };
        
        return count;
    }

    public decimal CalcInterestRate(int count)=>
        count switch
        {
            >= 100 => 12.5m,
            >= 96 => 15,
            >= 92 => 19,
            >= 88 => 22,
            >= 84 => 26,
            >= 80 => 30,
            _ => 0
        };
    
}