import {Component} from "react";

export default class Questionnaire extends Component {
    render() {
        return <div>
            <h1>Credit IO</h1>
                <form action="http://localhost:5155/Credit/MakeCalculation" method='post'>
                    <div>ФИО:</div>
                    <input name='fio' placeholder='ФИО' required/>
                    <div>
                        <div>Паспортные данные:</div>
                        <input name='passportSeries' placeholder='Серия' required minLength={4} maxLength={4}/>
                        <br/>
                        <input name='passportNumber' placeholder='Номер' required minLength={6} maxLength={6}/>
                        <br/>
                        <input name='passportGiven' placeholder='Кем выдан' required minLength={7} maxLength={40}/>
                        <br/>
                        <input name='passportGivenDate' placeholder='Дата выдачи' type='date' required min='1900-01-01' max='2023-01-01'/>
                        <br/>
                        <input name='passportRegistration' placeholder='Прописка' required minLength={5} maxLength={40}/>
                        <br/>
                    </div>
                    <div>Возраст:</div>
                    <input name='age' type='number' placeholder='Возраст' required min={18} max={100}/>
                    <div style={{display: 'flex'}}>
                    <div>Имеется ли судимость:</div>
                    <input name='isCriminal' type='checkbox' value='true'/>
                    </div>
                    <div>Сумма кредита:</div>
                    <input name='sum' type='number' step='0.01' placeholder='Сумма кредита' required min={1} max={100000000}/>
                    <div>Цель кредита:</div>
                        <select name='goal' required>
                            <option value='1'>Потребительский кредит</option>
                            <option value='2'>Недвижимость</option>
                            <option value='3'>Перекредитование</option>
                        </select>
                        <div>Трудоустройство:</div>
                            <select name='employment' required>
                                <option value='1'>Безработный</option>
                                <option value='2'>ИП</option>
                                <option value='3'>Договор ТК РФ</option>
                                <option value='4'>Без договора</option>
                                <option value='5'>Пенсионер</option>
                            </select>
                            <div style={{display:'flex'}}>
                            <div>Имеются ли другие кредиты:</div>
                            <input name='otherCredits' type='checkbox' value='true'/>
                            </div>
                            <div>Ваш залог:</div>
                                <select name='pledge' required>
                                    <option value='1'>Недвижимость</option>
                                    <option value='2'>Автомобиль</option>
                                    <option value='3'>Поручительство</option>
                                    <option value='4'>Без залога</option>
                                </select>
                                <div>Сколько лет авто,если ваш залог-авто</div>
                                <input type='number' name='autoAge'/>
                                <br/>
                                <br/>
                                <input type='submit' value='Проивзести расчет'/>
                </form>
        </div>
    }
} 