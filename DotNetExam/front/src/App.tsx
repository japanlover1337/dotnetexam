import React from 'react';
import {Route, Routes, BrowserRouter} from 'react-router-dom';
import Blank from "./Components/blank";

function App() {
    return (
        <BrowserRouter>
            <div className="App">
                <div>
                    <Routes>
                        <Route path='/' element={<Blank/>}/>
                    </Routes>
                </div>
            </div>
        </BrowserRouter>
    );
}

export default App;
