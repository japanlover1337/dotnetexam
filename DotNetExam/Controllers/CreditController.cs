using Microsoft.AspNetCore.Mvc;
using netExam.Logic;
using netExam.Models;

namespace netExam.Controllers;

[Route("[controller]/[action]")]
public class CreditController : Controller
{
    
    private readonly IsCriminalChecker.Checker _criminalStatusChecker;
    private readonly CountService _creditCalcs;

    public CreditController(IsCriminalChecker.Checker criminalStatusChecker, CountService creditCalcs)
    {
        _criminalStatusChecker = criminalStatusChecker;
        _creditCalcs = creditCalcs;
    }
    
    [HttpPost]
    public async Task<IActionResult> MakeCalculation(Questionnaire questionnaire)
    {
        var criminalStatusIsCorrect = await _criminalStatusChecker.IsReallyNotCrime(questionnaire.PassportNumber,questionnaire.isCriminal);
        if (!criminalStatusIsCorrect)
            return new JsonResult(new OverAllResult{
                Count = null,
                Result = false,
                InterestRate = null,
                Message="vi nas popitalis' obmanut' s vashei sudimost'u"
            });
        
        var count = _creditCalcs.Count(questionnaire);
        
        if (count < 80)
            return new JsonResult(new OverAllResult{
                Count = count,
                Result = false,
                Message = "no mi ne mozhem odobrit' vam credit",
                InterestRate = null
            });
        
        var creditRate = _creditCalcs.CalcInterestRate(count);
        return new JsonResult(new OverAllResult{
            Count = count, 
            Result = true,
            Message = "y credit dlya vas odobren",
            InterestRate = creditRate
        });
    }
}
