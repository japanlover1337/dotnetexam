﻿namespace netExam.Models;

public class OverAllResult
{
    public int? Count { get; set; }
    public bool Result { get; set; }
    public decimal? InterestRate { get; set; }
    public string Message { get; set; }
}