using System.ComponentModel.DataAnnotations;

namespace netExam.Models;
public class Questionnaire
{
    [Required]
    public string FIO { get; set; }
    [Required]
    public int PassportSeries { get; set; }
    
    [Required]
    public int PassportNumber { get; set; }

    [Required,MinLength(6),MaxLength(40)]
    public string PassportGiven { get; set; }
    [Required]
    public DateOnly PassportGivenDate { get; set; }
    [Required]
    public string PassportRegistration { get; set; }
    [Required]
    public int Age { get; set; }
    [Required]
    
    public bool isCriminal { get; set; }
    [Required]

    public decimal Sum { get; set; }
    [Required]
    public Goal Goal { get; set; }
    [Required]
    public Employment Employment { get; set; }
    [Required]

    public bool OtherCredits { get; set; }
    [Required]
    public Pledge Pledge { get; set; }
    [Required]
    public int AutoAge { get; set; }
}

public enum Goal
{
    Consumer = 1,
    RealEstate = 2,
    OnLending = 3
}

public enum Employment
{
    Unemployed = 1,
    Ip = 2,
    Tk = 3,
    NonTk = 4,
    Old=5
}

public enum Pledge
{
    RealEstate = 1,
    Auto = 2,
    Guarantee = 3,
    WithoutPledge=4,
}