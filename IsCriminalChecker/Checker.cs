﻿using System.Runtime.CompilerServices;

namespace IsCriminalChecker;

public class Checker
{
    public async Task<bool> IsReallyNotCrime(int passportNumber, bool isCriminal)
    {
        return (passportNumber % 10 == 7) == isCriminal;
    }
}